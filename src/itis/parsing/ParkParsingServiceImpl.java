package itis.parsing;

import itis.parsing.annotations.FieldName;
import itis.parsing.annotations.MaxLength;
import itis.parsing.annotations.NotBlank;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ParkParsingServiceImpl implements ParkParsingService {

    //Парсит файл в обьект класса "Park", либо бросает исключение с информацией об ошибках обработки
    @Override
    public Park parseParkData(String parkDatafilePath) throws ParkParsingException {

        try {

            HashMap<String, String> map = new HashMap<>();
            FileReader fileReader = new FileReader(parkDatafilePath);
            BufferedReader bufferReader = new BufferedReader(fileReader);
            bufferReader.readLine();
            String line;

            while (!(line = bufferReader.readLine()).trim().equals("***")) {
                String[] array = line.split(":");
                map.put(removeQuotes(array[0]), array[1]);
            }
            return parkDataFromMap(map);

        } catch (IOException | NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
            throw new ParkParsingException("Не удалось найти Park из файла данных", new ArrayList<>());
        }

    }

    private String removeQuotes(String string) {
        return string.replaceAll("\"", "");
    }


    private Park parkDataFromMap(HashMap<String, String> map) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

        Class<Park> parkClass = Park.class;
        Park parkInstance = parkClass.getDeclaredConstructor().newInstance();
        List<ParkParsingException.ParkValidationError> errors = new ArrayList<>();
        List<String> messages = new ArrayList<>();

        for (Field field : parkClass.getDeclaredFields()) {

            Annotation[] annotations = field.getAnnotations();
            String recognizedName = field.getName();
            boolean ifError = false;

            for (Annotation annotation : annotations) {

                if (annotation.annotationType() == FieldName.class) {
                    recognizedName = ((FieldName) annotation).value();
                }

                if (annotation.annotationType() == NotBlank.class && !map.containsKey(recognizedName)) {
                    String messageOfError = "Поле " + field.getName() + " не может быть пустым";
                    messages.add(messageOfError);
                    ParkParsingException.ParkValidationError parkValidationError = new ParkParsingException.ParkValidationError(field.getName(), messageOfError);
                    errors.add(parkValidationError);
                    ifError = true;
                }

                if (annotation.annotationType() == MaxLength.class && removeQuotes(map.get(recognizedName)).length() > ((MaxLength) annotation).value()) {
                    String errorMessage = "Размер не может быть больше чем " + ((MaxLength) annotation).value();
                    messages.add(errorMessage);
                    ParkParsingException.ParkValidationError parkValidationError = new ParkParsingException.ParkValidationError(field.getName(), errorMessage);
                    errors.add(parkValidationError);
                    ifError = true;
                }

            }

            if (!ifError) {
                field.set(parkInstance, removeQuotes(map.get(recognizedName)));
            }
        }

        if (errors.isEmpty()) {
            return parkInstance;
        } else {
            throw new ParkParsingException(messages.size() + " ошибок было найдено", errors);
        }
    }
}